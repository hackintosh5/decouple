package opensavvy.decouple.demo

import androidx.compose.runtime.Composable
import opensavvy.decouple.core.atom.text.Text
import opensavvy.decouple.core.layout.Column

@Composable
fun GettingStarted() = Column {
	Text("Welcome to OpenSavvy Decouple!")
	Text("In the future, this page will explain how to install the project.")
}
