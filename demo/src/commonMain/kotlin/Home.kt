package opensavvy.decouple.demo

import androidx.compose.runtime.Composable
import opensavvy.decouple.core.atom.text.Text

@Composable
fun Home() {
	Text("Welcome to OpenSavvy Decouple, the semantic visual library.")
}
